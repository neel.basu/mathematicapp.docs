To call a mathematica function you first need to declare it and make it available to your C++ code using `MATHEMATICA_DECLARE`
```cpp
MATHEMATICA_DECLARE(Table)
```
A bunch of functions are already been declared in "mathematica++/declares.h". Once a function has been declaed it can be declared in C++ code.

A symbol is declared through `mathematica::symbol` type which takes a string as the name of symbol identifier which ill be used in mathematica. 

```cpp
mathematica::symbol x("x");
```
The above symbol will be referenced as `x` inside mathematica as well as in C++. `mathematica::symbol` is declared in `mathematica++/symbol.h`

Matematica expressions are constructed using `mathematica::m` type
```cpp
mathematica::symbol i("i");
mathematica::symbol x("x");
mathematica::symbol y("y");
mathematica::m expr1 = Table(i, List(i, 1, 10));
mathematica::m expr2 = 2*Sin(x) + 3*Cos(y);
```

An expression or a symbol is sent to matheatica via `mathematica::connector` 
```cpp
mathematica::connector shell;
shell << expr2;
```
After evaluating the expression Mathematica may return a result which is captured in `mathematica::value`

```cpp
mathematica::value result;
shell << Table(i, List(i, 1, 10));
shell >> result;
```
`mathematica::value` is actually a `shared_ptr` of `mathematica::token`. The result returned by mathematica is parsed and a tree of such `token`s are constructed. The root node of that tree is returned in `result`. `mathematica::value` and `mathematica::token` is declared in `mathematica++/token.h`