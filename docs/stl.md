# STL containers
STL containers like `std::vector` can be passed directly to mathematica functions 
```cpp
value result;
std::vector<int> list = {1,2,3,4,5};
shell << Total(list);
shell >> result;
std::cout << result << std::endl; // Prints 15
```
Similarly result returned by mathematica can be casted to a composite stl type
```cpp
value result;
typedef std::vector<std::vector<int>> ivv_type;
shell << FactorInteger(2434500);
shell >> result;
ivv_type prime_powers = mathematica::cast<ivv_type>(result);
for(auto pp: prime_powers){
    std::cout << pp[0] << " ^ " << pp[1] << std::endl;
}
// Prints the following output
// 2 ^ 2
// 3 ^ 2
// 5 ^ 3
// 541 ^ 1

```
Not limited to `std::vector`
```cpp
typedef std::vector<std::pair<int, int>> ipv_type;
shell << FactorInteger(2434500);
shell >> result;
ipv_type prime_powers = mathematica::cast<ipv_type>(result);
```
