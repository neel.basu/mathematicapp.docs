# mathematica::connector
A connector is used to send commands to and get results from mathematica. It is used much like `std::cout` via `operator<<` and `operator>>` overloads.

```cpp
mathematica::connector shell;
shell << 2*Cos(x);
shell >> result;
```

# mathematica::symbol
algebric symbols like `x`, `y` etc.. are constructed using `symbol`.

```cpp
mathematica::symbol x("x");
mathematica::symbol y("y");
shell << 2*Cos(x) + 3*Sin(y);
```

# mathematica::m
Composite mathematica expressions like `a Sin[x] + b Cos[y]` are constructed using `m`. `+,-,*,/,==,!=,<,>,<=,>=,&&,||` operators are overloaded to behave as mathematica functions `Plus[], Minus[], Times[], Divide[], Equal[], Unequal[], Less[], Greater[], LessEqual[], GreaterEqual[], And[], Or[]` respectively.

```cpp
mathematica::m expr = 2*Sin(x) + 3*Cos(y); // Equivalent to 2 Sin[x] + 3 Cos[y] in mathematica
```