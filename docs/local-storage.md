Instead of fetching output of a symbolic computation it can be held in a local storage with `mathematica::connector::save()` which can latter be loaded and used in other computations.
```cpp
symbol x("x");
value result;

shell.save() << Integrate(Sin(x), x);
shell << shell.last() + Cos(x);
shell >> result;
```
`save()` creates a unique identifier using current time stamp to store the variable in a lookup table **inside mathematica**. `last()` returns a `mathematica::m` object that wraps a reference to the last saved object. Instead of assigning an arbitrary name `save()` can be called by using an user specified string argument.

```cpp
shell.save("pluto") << Integrate(Sin(x), x);
std::cout << shell.last_key() << std::endl; // Prints pluto
shell << shell.ref("pluto")+Cos(x);
shell >> result;
std::cout << result << std::endl; // Prints 0
```
Use `last_key()` to get the identifier as string
```cpp
symbol x("x");
value result;
    
std::string left_key, right_key;
     
shell.save() << Integrate(2*x, x);
left_key = shell.last_key();
shell.save() << Integrate(-2/Power(x,3), x);
right_key = shell.last_key();
shell << shell.ref(left_key)*shell.ref(right_key);
shell >> result;
std::cout << result << std::endl; // Prints 1
```
