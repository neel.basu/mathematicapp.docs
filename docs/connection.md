# Using the default arguments

Connect to mathematica via `mathematica::connector` with or without `argc` and `argv` command line arguments as documented in [Mathematica Website][1].
```cpp
mathematica::connector shell;
```
The above will create a connection to mathematica using the default arguments shown below.
```
# Windows -linkname math -mathlink
# Linux   -linkname math -mathlink
# Apple   -linkname /Applications/Mathematica.app/Contents/MacOS/MathKernel -mathlink
```
# Specifying connection parameters

`mathematica::connector` have two different constructor overloads that support string and character array command lien arguments

```cpp
mathematica::connector(argc, argv);
mathematica::connector(const std::string& argv);
```

A connection can be setup with an existing link of type `MLINK` or `WSLINK`

```cpp
mathematica::connector(driver::link(link))
```

# Library

When using LibraryLink a connection object can be set up through `mathematica::transport`. However that is not used directly, instead either of the two subclasses `mtransport` or `wtransport` is used.

```cpp
mathematica::mtransport(WolframLibraryData data, int argc, MArgument* argv, MArgument res, std::string lib_name="");
mathematica::wtransport(WolframLibraryData data, link_type link, std::string lib_name="")
```



[1]: http://reference.wolfram.com/language/ref/c/WSOpenArgcArgv.html